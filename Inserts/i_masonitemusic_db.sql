INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('1', 'JohnSmith', 'dougeverett@gmail.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('2', 'HenryFord', 'hford@hotmail.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('3', 'JohnDover', 'ddover@yahoo.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('4', 'engineer5', 'webmaster@google.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('5', 'billiejean', 'bjean1@gmail.com', 'secret');

INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Michael Jackson', 1);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Imagine Dragons', 2);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('X-Ambassadors', 3);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Nick Jonas', 4);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Eminem', 5);

INSERT INTO GENRE(Genre, GenreID)
VALUES('Rock', 1);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Pop', 2);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Hip Hop', 3);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Rap', 4);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Jazz', 5);

INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Last year was complicated', 1, 4);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Thriller', 2, 1);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Recovery', 3, 5);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('VHS', 4, 3);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Origins', 5, 2);

INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(1, 'Thriller', 1, 2, '', 1);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(2, 'Levels', 4, 2, '', 4);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(3, 'Whatever it takes', 2, 1, '', 5);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(4, 'Beautiful', 5, 3, '', 3);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(5, 'Low Life', 3, 1, '', 4);

INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(1, 'Rock and Roll', 1,1);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(2,'Top 100', 2,2);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(3,'My favorites', 3,2);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(4,'Relaxing Music', 3,3);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(5,'My Gym Playlist', 4,4);

INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,1,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,2,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,3,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(2,1,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(3,1,sysdate);