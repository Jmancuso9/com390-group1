DROP TABLE PLAYED;
DROP TABLE PLAYLISTS;
DROP TABLE SONGS;
DROP TABLE GENRE;
DROP TABLE ALBUMS;
DROP TABLE ARTISTS;
DROP TABLE MUSICUSERS;
DROP TABLE SUGGESTED;


CREATE TABLE MUSICUSERS (
    UserID NUMBER(4),
    UserName VARCHAR2(30),
    Email VARCHAR2(30),
    UserPassword VARCHAR2(20),
    CONSTRAINT userID_pk PRIMARY KEY(UserID)
);

INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('1', 'JohnSmith', 'dougeverett@gmail.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('2', 'HenryFord', 'hford@hotmail.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('3', 'JohnDover', 'ddover@yahoo.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('4', 'engineer5', 'webmaster@google.com', 'secret');
INSERT INTO MUSICUSERS(UserId, UserName, Email, UserPassword)
VALUES ('5', 'billiejean', 'bjean1@gmail.com', 'secret');



CREATE TABLE ARTISTS (
    ArtistName VARCHAR2(30),
    ArtistID NUMBER(4),
    CONSTRAINT artistID_pk PRIMARY KEY(ArtistID)
);

INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Michael Jackson', 1);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Imagine Dragons', 2);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('X-Ambassadors', 3);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Nick Jonas', 4);
INSERT INTO ARTISTS (ArtistName, ArtistID)
VALUES('Eminem', 5);

CREATE TABLE GENRE (
    Genre VARCHAR2(20),
    GenreID NUMBER(4),
    CONSTRAINT genreID_pk PRIMARY KEY(GenreID)
);

INSERT INTO GENRE(Genre, GenreID)
VALUES('Rock', 1);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Pop', 2);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Hip Hop', 3);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Rap', 4);
INSERT INTO GENRE(Genre, GenreID)
VALUES('Jazz', 5);


CREATE TABLE ALBUMS (
    AlbumName VARCHAR2(255),
    AlbumID NUMBER(4),
    ArtistID NUMBER(4),
    CONSTRAINT albumID_pk PRIMARY KEY(AlbumID)
);

INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Last year was complicated', 1, 4);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Thriller', 2, 1);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Recovery', 3, 5);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('VHS', 4, 3);
INSERT INTO ALBUMS (AlbumName, AlbumID, ArtistID)
VALUES('Origins', 5, 2);

CREATE TABLE SONGS (
    SongID NUMBER(4),
    SongTitle VARCHAR(30),
    ArtistID NUMBER(4),
    GenreID NUMBER(4),
    FileName VARCHAR2(30),
    AlbumID NUMBER(4),
    CONSTRAINT songID_pk PRIMARY KEY(SongID),
    CONSTRAINT songs_genreID_fk FOREIGN KEY(GenreID) 
        REFERENCES GENRE(GenreID) ON DELETE SET NULL,
    CONSTRAINT songs_albumID_fk FOREIGN KEY(AlbumID) 
        REFERENCES ALBUMS(AlbumID) ON DELETE SET NULL,
    CONSTRAINT songs_artistID_fk FOREIGN KEY(ArtistID) 
        REFERENCES ARTISTS(ArtistID) ON DELETE CASCADE
);

INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(1, 'Thriller', 1, 2, '', 1);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(2, 'Levels', 4, 2, '', 4);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(3, 'Whatever it takes', 2, 1, '', 5);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(4, 'Beautiful', 5, 3, '', 3);
INSERT INTO SONGS (SongID, SongTitle, ArtistID, GenreID, FileName, AlbumID)
VALUES(5, 'Low Life', 3, 1, '', 4);



CREATE TABLE PLAYLISTS (
    PlaylistID NUMBER(4),
    PlaylistName VARCHAR(30),
    SongID NUMBER(4),
    UserID NUMBER(4),
    CONSTRAINT playlistID_pk PRIMARY KEY(PlaylistID),
    CONSTRAINT playlists_songID_fk FOREIGN KEY(SongID) 
        REFERENCES SONGS(SongID) ON DELETE SET NULL,
    CONSTRAINT playlists_userID_fk FOREIGN KEY(UserID) 
        REFERENCES MUSICUSERS(UserID)  ON DELETE CASCADE  
);

INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(1, 'Rock and Roll', 1,1);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(2,'Top 100', 2,2);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(3,'My favorites', 3,2);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(4,'Relaxing Music', 3,3);
INSERT INTO PLAYLISTS(PlaylistID, PlaylistName, SongID, UserID)
VALUES(5,'My Gym Playlist', 4,4);


CREATE TABLE PLAYED (
    SongID NUMBER(4),
    UserID NUMBER(4),
    PlayedAt DATE,
    CONSTRAINT played_songID_fk FOREIGN KEY(SongID) 
        REFERENCES SONGS(SONGID) ON DELETE CASCADE,
    
    CONSTRAINT artists_userID_fk FOREIGN KEY(UserID) 
        REFERENCES MUSICUSERS(UserID) ON DELETE CASCADE
);



INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,1,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,2,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(1,3,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(2,1,sysdate);
INSERT INTO PLAYED(SongId, UserID, PlayedAt)
VALUES(3,1,sysdate);

CREATE TABLE SUGGESTED (
    SuggestedID NUMBER(4),
    UserID NUMBER(4),
    SongID Number(4),
    CONSTRAINT suggested_songID_fk FOREIGN KEY(SongID) 
        REFERENCES SONGS(SONGID) ON DELETE SET NULL,
    
    CONSTRAINT suggested_userID_fk FOREIGN KEY(UserID) 
        REFERENCES MUSICUSERS(UserID) ON DELETE SET NULL
);

INSERT INTO SUGGESTED(SuggestedID, UserID, SongID)
VALUES(1,1,1);
INSERT INTO SUGGESTED(SuggestedID, UserID, SongID)
VALUES(2,2,2);
INSERT INTO SUGGESTED(SuggestedID, UserID, SongID)
VALUES(3,3,3);
INSERT INTO SUGGESTED(SuggestedID, UserID, SongID)
VALUES(4,4,4);
INSERT INTO SUGGESTED(SuggestedID, UserID, SongID)
VALUES(5,5,5);
