from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

# Create a new AutomapBase Class
Base = automap_base()

# Create a Database Engine
# By default, these operations exclude the SYSTEM and SYSAUX tablespaces

# Create a database engine
connection_string = "oracle://jmancuso2:0342966"
connection_string += "@(DESCRIPTION = (LOAD_BALANCE=on) (FAILOVER=ON)"
connection_string += "(ADDRESS = (PROTOCOL = TCP)(HOST = 10.0.20.22)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ORCL.campus.sjcny.edu)))"
engine = create_engine(connection_string, exclude_tablespaces=["SYSAUX"])
# For the db_connection.py file:
# Create a database engine
# connection_string = "oracle://jmancuso2:0342966"
# connection_string += "@(DESCRIPTION = (LOAD_BALANCE=on) (FAILOVER=ON)"
# connection_string += "(ADDRESS = (PROTOCOL = TCP)(HOST = 10.0.20.22)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ORCL.campus.sjcny.edu)))"
# engine = s.create_engine(connection_string, exclude_tablespaces=["SYSAUX"])


# reflect the tables
Base.prepare(engine, reflect=True)

# Map a new Python class to a table in our database
Movie = Base.classes.mm_movie

# Create a new session that will manage our database connection
session = Session(engine)

# Return all Movies
def get_movies():
    rs = session.query(Movie).all()
    result_list = []
    for movie in rs:
        result_list.append((movie.__dict__))
        # Thanks https://stackoverflow.com/questions/1958219/convert-sqlalchemy-row-object-to-python-dict

    return result_list

# READ a Movie record
def get_movie(id):
    m = session.query(Movie).get(id)
    return m.__dict__

# CREATE a Movie record
def create_movie(cat_id, qty, id, value, title):
    m = Movie(
        movie_cat_id=cat_id,
        movie_qty=qty,
        movie_id=id,
        movie_value=value,
        movie_title=title
    )
    session.add(m)
    session.commit()

# UPDATE a Movie record
def update_movie(cat_id, qty, id, value, title):
    m = session.query(Movie).get(id)
    m.movie_cat_id = cat_id
    m.movie_qty = qty
    m.movie_id = id
    m.movie_value = value
    m.movie_title = title
    session.commit()

# DELETE a Movie record
def delete_movie(id):
    m = session.query(Movie).get(id)
    session.delete(m)
    session.commit()