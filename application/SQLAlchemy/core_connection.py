import sqlalchemy as s

# Notes about Oracle Connections: https://docs.sqlalchemy.org/en/latest/dialects/oracle.html
from sqlalchemy import create_engine, MetaData, Table, text

# Create a database engine
engine = create_engine('oracle://jmancuso2:0342966@127.0.0.1:1521', exclude_tablespaces=["SYSAUX"])

# Create a MetaData object to store information about the database
metadata = MetaData(bind=engine)

# Reflect all of the tables in the database
metadata.reflect()

# Create a python Table object using a table in the database metadata
movies = metadata.tables['mm_movie']

# Return all Movies
def get_movies():
    s = movies.select()
    conn = engine.connect()
    rs = conn.execute(s)
    result_list = []
    for row in rs:
        result_list.append(dict(row))

    return result_list

# READ a Movie record
def get_movie(id):
    s = movies.select().where(movies.c.movie_id == id)
    conn = engine.connect()
    rs = conn.execute(s).fetchone()
    return dict(rs)

# CREATE a Movie record
def create_movie(cat_id, qty, id, value, title):
    stmt = movies.insert().values(
        movie_cat_id=cat_id,
        movie_qty=qty,
        movie_id=id,
        movie_value=value,
        movie_title=title
    )

    conn = engine.connect()
    conn.execute(stmt)

# Update a Movie record
def update_movie(cat_id, qty, id, value, title):
    stmt = movies.update().where(
        movies.c.id == id
    ).values(
        movie_cat_id=cat_id,
        movie_qty=qty,
        movie_id=id,
        movie_value=value,
        movie_title=title
    )
    conn = engine.connect()
    conn.execute(stmt)

# Delete a Movie Record
def delete_movie(id):
    stmt = movies.delete().where(movies.c.id == id)
    conn = engine.connect()
    conn.execute(stmt)