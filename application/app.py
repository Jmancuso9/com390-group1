from flask import Flask, jsonify, request, render_template 
from flask_sqlalchemy import SQLAlchemy
import json

from sqlalchemy.ext.declarative import DeclarativeMeta

connection_string = "oracle://jmancuso:0342966"
connection_string += "@(DESCRIPTION = (LOAD_BALANCE=on) (FAILOVER=ON)"
connection_string += "(ADDRESS = (PROTOCOL = TCP)(HOST = 10.0.20.22)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ORCL.campus.sjcny.edu)))"

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = connection_string

sql = SQLAlchemy(app)


class Song(sql.Model):
    __tablename__ = 'SONGS'
    songid = sql.Column(sql.Integer, primary_key=True)
    songtitle = sql.Column(sql.String(25))
    artistid = sql.Column(sql.String(25))
    genreid = sql.Column(sql.Integer)
    albumid = sql.Column(sql.String(25))
    filename = sql.Column(sql.String(25))

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def to_json(self):
        """
        Jsonify the sql alchemy query result.
        """
        cls = self.__class__
        inst = self
        convert = dict()
        # add your coversions for things like datetime's 
        # and what-not that aren't serializable.
        d = dict()
        for c in cls.__table__.columns:
            v = getattr(inst, c.name)
            if c.type in convert.keys() and v is not None:
                try:
                    d[c.name] = convert[c.type](v)
                except:
                    d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
            elif v is None:
                d[c.name] = str()
            else:
                d[c.name] = v
        
        return jsonify(d)
        return json.dumps(d)
        
class MusicUser(sql.Model):
    __tablename__ = 'MUSICUSERS'
    userid = sql.Column(sql.Integer, primary_key=True)
    username = sql.Column(sql.String(25))
    email = sql.Column(sql.String(25))
    userpassword = sql.Column(sql.String(25))

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def to_json(self):
        """
        Jsonify the sql alchemy query result.
        """
        cls = self.__class__
        inst = self
        convert = dict()
        # add your coversions for things like datetime's 
        # and what-not that aren't serializable.
        d = dict()
        for c in cls.__table__.columns:
            v = getattr(inst, c.name)
            if c.type in convert.keys() and v is not None:
                try:
                    d[c.name] = convert[c.type](v)
                except:
                    d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
            elif v is None:
                d[c.name] = str()
            else:
                d[c.name] = v
        
        return jsonify(d)
        return json.dumps(d)
        

#SONGS ---------------------------------------------------------------------------------------------------------------------------------------------------------
#Get all songs
@app.route("/", methods= ["GET"]) # /songs is the URL you go to in the website.
def home():
    return render_template('index.html'), 200

#SONGS ---------------------------------------------------------------------------------------------------------------------------------------------------------
#Get all songs
@app.route("/songs", methods= ["GET"]) # /songs is the URL you go to in the website.
def get_songs():
    song_list = Song.query.all()
    row = []
    for song in song_list:
        row.append(song.as_dict())
    # return jsonify({'songs': song_list}), 200     # converts dictionary returned into a json response to be sent to the browser like you see in the browser window.
    print(row)
    return jsonify({'songs': row}), 200     # converts dictionary returned into a json response to be sent to the browser like you see in the browser window.
    # when you pass in multiple records. wrap it here like {'wrapper': song_list}

#Get a single song
@app.route("/songs/<id>", methods= ["GET"]) # <id> is a way to pass in a "parameter"
def get_song(id): # "id" has to match <id>. so pass in poop if you have <poop>
    # song_list = db.get_song(id) 
    try:
        song_list = Song.query.filter_by(songid=id).first()
        print(song_list)
        return song_list.to_json(), 200
    except:
        return jsonify({'error': 'record does not exist'}), 404

# Create a song
"""
REQUEST
{
    "songid": 100,
    "songtitle": "I am sunshine",
    "artistid": 2,
    "genreid": 2,
    "filename": "",
    "albumid": 2
}
"""
@app.route("/songs", methods= ["POST"])
def create_song():
    m_songid = request.form['songid']
    m_songtitle = request.form['songtitle']
    m_artistid = request.form['artistid']
    m_genreid = request.form['genreid']
    m_filename = request.form['filename']
    m_albumid = request.form['albumid']

    new_song = Song(
        songid= m_songid,
        songtitle= m_songtitle,
        artistid= m_artistid,
        genreid= m_genreid,
        filename= m_filename,
        albumid= m_albumid
    )

    try:
        sql.session.add(new_song)
        sql.session.commit()
    except:
        return jsonify({'error': 'record already exists with the id of: {}'.format(m_songid)}), 200

    song_list = Song.query.filter_by(songid=m_songid).first()
    print(song_list)
    return song_list.to_json(), 200


# Update a song
"""
REQUEST: 
{
    "songtitle": "peppa pig",
    "artistid": 2,
    "genreid": 2,
    "filename": "",
    "albumid": 2
}
"""
@app.route("/songs/<m_songid>", methods=["PUT"])
def update_song(m_songid):
    # m_songid = request.json['songid']
    m_songtitle = request.form['songtitle']
    m_artistid = request.form['artistid']
    m_genreid = request.form['genreid']
    m_filename = request.form['filename']
    m_albumid = request.form['albumid']

    try:
        Song.query.filter_by(songid=m_songid).update({
            'songid': m_songid,
            'songtitle': m_songtitle,
            'artistid': m_artistid,
            'genreid': m_genreid,
            'filename': m_filename,
            'albumid': m_albumid
        })
            # result = db.update_song(id, m_songtitle, m_artistid, m_genreid, m_filename, m_albumid)
        result = Song.query.filter_by(songid=m_songid).first()

        return result.to_json(), 200  
    except:
        return jsonify({'error': 'record does not exist'}), 404


@app.route("/songs/<id>", methods= ["DELETE"])
def delete_song(id):
    try:
        song = Song.query.filter_by(songid=id).first()
        sql.session.delete(song)
        sql.session.commit()
        print('finished calling')
    
        return song.to_json(), 200
    except:
        return jsonify({'error': 'record does not exist'}), 404

#MUSICUSERS ---------------------------------------------------------------------------------------------------------------------------------------------------------
#Get all musicusers
@app.route("/musicusers", methods= ["GET"]) 
def get_musicusers():
    musicusers = MusicUser.query.all()
    row = []
    for user in musicusers:
        row.append(user.as_dict())
    # return jsonify({'songs': song_list}), 200     # converts dictionary returned into a json response to be sent to the browser like you see in the browser window.
    print(row)
    return jsonify({'users': row}), 200  

#Get a single musicuser
@app.route("/musicusers/<id>", methods= ["GET"]) 
def get_musicuser(id): 
    # song_list = db.get_song(id) 
    try:
        user = MusicUser.query.filter_by(userid=id).first()
        print(user)
        return user.to_json(), 200
    except:
        return jsonify({'error': 'record does not exist'}), 404

# Create a musicuser
@app.route("/musicusers", methods= ["POST"])
def create_musicuser():
    m_userid = request.json['userid']
    m_username = request.json['username']
    m_email = request.json['email']
    m_password = request.json['password']

    new_user = MusicUser(userid=m_userid, username= m_username, email= m_email, userpassword=m_password)
    
    try:
        sql.session.add(new_user)
        sql.session.commit()
    except:
        return jsonify({'error': 'record already exists with the id of: {}'.format(m_userid)}), 200

    user = MusicUser.query.filter_by(userid=m_userid).first()
    print(user)
    return user.to_json(), 200


# Update a musicuser
@app.route("/musicusers/<id>", methods= ["PUT"])
def update_musicuser(id):
    m_username = request.json['username']
    m_email = request.json['email']
    m_password = request.json['password']

    try:
        MusicUser.query.filter_by(userid=id).update({
            'username': m_username,
            'email': m_email,
            'userpassword': m_password,
        })

        result = MusicUser.query.filter_by(userid=id).first()

        return result.to_json(), 200  
    except:
        return jsonify({'error': 'record does not exist'}), 404


#Delete a musicuser
@app.route("/musicusers/<id>", methods= ["DELETE"])
def delete_musicuser(id):
    try:
        user = MusicUser.query.filter_by(userid=id).first()
        sql.session.delete(user)
        sql.session.commit()
    
        return user.to_json(), 200
    except:
        return jsonify({'error': 'record does not exist'}), 404

# FILTER
#Delete played
@app.route("/songs/artists/<artist_id>", methods= ["GET"])
def filter_songs(artist_id):
    result = Song.query.filter_by(artistid=artist_id).all()

    row = []
    for record in result:
        row.append(record.as_dict())
    # return jsonify({'songs': song_list}), 200     # converts dictionary returned into a json response to be sent to the browser like you see in the browser window.
    print(row)
    return jsonify({'songs': row}), 200  

@app.route("/recommendations/<user_id>", methods=["GET"])
def get_recommendations(user_id):
    sql.session.execute("""
    begin
        suggested_song({});
    end;
    """.format(user_id))

    sql.session.commit()

    result = sql.session.execute("SELECT * FROM suggested where userid = '{}'".format(user_id)).fetchall()
    return jsonify({'songs': [dict(row) for row in result]}),200  


if __name__ == "__main__":
    app.run(debug =True)