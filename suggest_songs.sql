-- suggested songs
CREATE OR REPLACE PROCEDURE suggested_song (
    lv_userid   musicusers.userid%TYPE := 1
) IS
BEGIN
    DECLARE
        rec_played_row   played%rowtype;
    BEGIN
        DELETE FROM suggested
        WHERE
            userid = lv_userid;

        FOR c IN (
            SELECT DISTINCT
                *
            INTO rec_played_row
            FROM
                played
            WHERE
                userid = lv_userid
                AND ROWNUM <= 3
        ) LOOP
            INSERT INTO suggested (
                suggestedid,
                userid,
                songid
            ) VALUES (
                floor(dbms_random.value(10,10000) ),
                lv_userid,
                c.songid
            );

        END LOOP;

    END;

    DECLARE
        lv_songid   songs.songid%TYPE;
        rec_songs   songs%rowtype;
    BEGIN
        SELECT
            songid
        INTO lv_songid
        FROM
            played
        WHERE
            ROWNUM = 1
        ORDER BY
            ( dbms_random.value );

        FOR c IN (
            SELECT DISTINCT
                *
            INTO rec_songs
            FROM
                songs
            WHERE
                genreid = (
                    SELECT
                        genreid
                    FROM
                        songs
                    WHERE
                        songid = lv_songid
                )
        ) LOOP
            INSERT INTO suggested (
                suggestedid,
                userid,
                songid
            ) VALUES (
                floor(dbms_random.value(10,10000) ),
                lv_userid,
                c.songid
            );

        END LOOP;

    END;

END;
/