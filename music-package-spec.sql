CREATE OR REPLACE PACKAGE music_library_pkg IS
--    TYPE mm_movie_tapi_rec IS RECORD (
--        movie_cat_id mm_movie.movie_cat_id%TYPE,
--        movie_qty mm_movie.movie_qty%TYPE,
--        movie_id mm_movie.movie_id%TYPE,
--        movie_value mm_movie.movie_value%TYPE,
--        movie_title mm_movie.movie_title%TYPE
--    );
--    TYPE mm_movie_tapi_tab IS
--        TABLE OF mm_movie_tapi_rec;

-- Movie Table Procedure Specs
-- INSERT / CREATE
    PROCEDURE insert_musicusers (
        p_userid         IN musicusers.userid%TYPE,
        p_username       IN musicusers.username%TYPE DEFAULT NULL,
        p_email          IN musicusers.email%TYPE DEFAULT NULL,
        p_userpassword   IN musicusers.userpassword%TYPE DEFAULT NULL
    );


--READ MUSICUSERS

    PROCEDURE get_musicusers (
        p_userid           IN musicusers.userid%TYPE,
        p_rec_musicusers   OUT musicusers%rowtype
    );
    --UPDATE MUSICUSERS

    PROCEDURE update_musicusers (
        p_userid         IN musicusers.userid%TYPE,
        p_username       IN musicusers.username%TYPE,
        p_email          IN musicusers.email%TYPE,
        p_userpassword   IN musicusers.userpassword%TYPE
    );
    --DELETE MUSICUSERS

    PROCEDURE delete_musicusers (
        p_userid   IN musicusers.userid%TYPE
    );
        
    
    --CREATE ARTISTS

    PROCEDURE insert_artists (
        p_artistname   IN artists.artistname%TYPE DEFAULT NULL,
        p_artistid     IN artists.artistid%TYPE
    );
    --READ ARTISTS

    PROCEDURE get_artists (
        p_artistid      IN artists.artistid%TYPE,
        p_rec_artists   OUT artists%rowtype
    );
    --UPDATE ARTISTS

    PROCEDURE update_artists (
        p_artistid     IN artists.artistid%TYPE,
        p_artistname   IN artists.artistname%TYPE
    );
    --DELETE ARTISTS

    PROCEDURE delete_artists (
        p_artistid   IN artists.artistid%TYPE
    );
        
    --CREATE GENRE

    PROCEDURE insert_genre (
        p_genre     IN genre.genre%TYPE DEFAULT NULL,
        p_genreid   IN genre.genreid%TYPE
    );
    --READ GENRE

    PROCEDURE get_genre (
        p_genreid     IN genre.genreid%TYPE,
        p_rec_genre   OUT genre%rowtype
    );
    --UPDATE GENRE

    PROCEDURE update_genre (
        p_genre     IN genre.genre%TYPE,
        p_genreid   IN genre.genreid%TYPE
    );
    --DELETE GENRE

    PROCEDURE delete_genre (
        p_genreid   IN genre.genreid%TYPE
    );      

    --CREATE ALBUMS

    PROCEDURE insert_albums (
        p_albumname   IN albums.albumname%TYPE DEFAULT NULL,
        p_albumid     IN albums.albumid%TYPE,
        p_artistid    IN artists.artistid%TYPE
    );
    --READ ALBUMS

    PROCEDURE get_albums (
        p_albumid     IN albums.albumid%TYPE,
        p_rec_album   OUT albums%rowtype
    );
    --UPDATE ALBUMS

    PROCEDURE update_albums (
        p_albumname   IN albums.albumname%TYPE,
        p_albumid     IN albums.albumid%TYPE,
        p_artistid    IN albums.artistid%TYPE
    );
    --DELETE ALBUMS

    PROCEDURE delete_albums (
        p_albumid   IN albums.albumid%TYPE
    );
--    
    
    --CREATE SONGS

    PROCEDURE insert_songs (
        p_songid      IN songs.songid%TYPE,
        p_songtitle   IN songs.songtitle%TYPE DEFAULT NULL,
        p_artistid    IN songs.artistid%TYPE,
        p_genreid     IN songs.genreid%TYPE,
        p_filename    IN songs.filename%TYPE DEFAULT NULL,
        p_albumid     IN songs.albumid%TYPE
    );
    --READ SONGS

    PROCEDURE get_songs (
        p_songid      IN songs.songid%TYPE,
        p_rec_songs   OUT songs%rowtype
    );
    --UPDATE SONGS

    PROCEDURE update_songs (
        p_songid      IN songs.songid%TYPE,
        p_songtitle   IN songs.songtitle%TYPE,
        p_artistid    IN songs.artistid%TYPE,
        p_genreid     IN songs.genreid%TYPE,
        p_filename    IN songs.filename%TYPE,
        p_albumid     IN songs.albumid%TYPE
    );
    --DELETE SONGS

    PROCEDURE delete_songs (
        p_songid   IN songs.songid%TYPE
    );
    
    --CREATE PLAYLISTS

    PROCEDURE insert_playlists (
        p_playlistid     IN playlists.playlistid%TYPE,
        p_playlistname   IN playlists.playlistname%TYPE DEFAULT NULL,
        p_songid         IN playlists.songid%TYPE,
        p_userid         IN playlists.userid%TYPE
    );
    --READ PLAYLISTS

    PROCEDURE get_playlists (
        p_playlistid      IN playlists.playlistid%TYPE,
        p_rec_playlists   OUT playlists%rowtype
    );
    --UPDATE PLAYLISTS

    PROCEDURE update_playlists (
        p_playlistid     IN playlists.playlistid%TYPE,
        p_playlistname   IN playlists.playlistname%TYPE,
        p_songid         IN playlists.songid%TYPE,
        p_userid         IN playlists.userid%TYPE
    );
    --DELETE PLAYLISTS

    PROCEDURE delete_playlists (
        p_playlistid   IN playlists.playlistid%TYPE
    );
    
    --CREATE PLAYED

    PROCEDURE insert_played (
        p_songid     IN played.songid%TYPE DEFAULT NULL,
        p_userid     IN played.userid%TYPE DEFAULT NULL,
        p_playedat   IN played.playedat%TYPE DEFAULT NULL
    );
--    --READ PLAYED

    PROCEDURE get_played (
        p_songid       IN played.songid%TYPE,
        p_rec_played   OUT played%rowtype
    );
--    --UPDATE PLAYED

    PROCEDURE update_played (
        p_songid     IN played.songid%TYPE,
        p_userid     IN played.userid%TYPE,
        p_playedat   IN played.playedat%TYPE
    );
--    --DELETE PLAYED
--

    PROCEDURE delete_played (
        p_songid   IN played.songid%TYPE
    );
-- End Movie Table Procedures

-- Addtional Tables Should be listed below

END music_library_pkg; -- End Movie API

/