SET ECHO ON
/

-- CREATE MUSICUSERS

BEGIN
    music_library_pkg.insert_musicusers(100,'JaneDoe','janedoe@gmail.com','secret');
END;
/

SELECT
    *
FROM
    musicusers;

 -- READ MUSICUSERS

DECLARE
    lv_userid        musicusers.userid%TYPE := 4;
    rec_musicusers   musicusers%rowtype;
BEGIN
    music_library_pkg.get_musicusers(lv_userid,rec_musicusers);
    dbms_output.put_line('User ID: '
                           || rec_musicusers.userid
                           || ' Username: '
                           || rec_musicusers.username
                           || ' Email: '
                           || rec_musicusers.email);

END;
/

-- UPDATE MUSICUSERS

BEGIN
    music_library_pkg.update_musicusers(100,'John Doe','johndoe@gmail.com','secret');
END;
/

SELECT
    *
FROM
    musicusers
WHERE
    userid = 100;

-- DELETE MUSICUSERS

BEGIN
    music_library_pkg.delete_musicusers(2);
END;
/

SELECT
    *
FROM
    musicusers;

--

-- CREATE ARTISTS

BEGIN
    music_library_pkg.insert_artists('The Beatles',15);
END;
/

SELECT
    *
FROM
    artists;

-- READ ARTISTS

DECLARE
    lv_artistid   artists.artistid%TYPE := 1;
    rec_artists   artists%rowtype;
BEGIN
    music_library_pkg.get_artists(lv_artistid,rec_artists);
    dbms_output.put_line('Artist ID: '
                           || rec_artists.artistid
                           || ' Artist Name: '
                           || rec_artists.artistname);

END;
/

-- UPDATE ARTISTS
BEGIN 
music_library_pkg.update_artists(5,'Billy Joel');
END;
/
SELECT * FROM ARTISTS
    WHERE ArtistID = 5;

-- DELETE ARTISTS
BEGIN 
    music_library_pkg.delete_artists(3);
END;
/
SELECT * FROM ARTISTS;


-- CREATE GENRE
BEGIN
  music_library_pkg.insert_genre('Dance Music', 20);
END;
/
SELECT * FROM GENRE;

-- READ GENRE
DECLARE
  lv_GenreID GENRE.GenreID%TYPE := 1;
  rec_genre GENRE%ROWTYPE;
BEGIN
    music_library_pkg.get_genre(lv_GenreID, rec_genre);
    DBMS_OUTPUT.PUT_LINE('Genre ID: ' || rec_genre.GenreID ||  ' Genre Name: ' || rec_genre.Genre);
END;
/

-- UPDATE GENRE
BEGIN
music_library_pkg.update_artists(4, 'Country');
END;
/
SELECT * FROM GENRE
    WHERE GenreID = 4;

-- DELETE GENRE
BEGIN 
    music_library_pkg.delete_genre(1);
END;
/

SELECT * FROM GENRE;


-- CREATE ALBUMS
BEGIN
  music_library_pkg.insert_albums('Sgt. Peppers Lonely Hearts Club Band', 12, 15);
END;
/
SELECT * FROM ALBUMS;

-- READ ALBUMS
DECLARE
  lv_AlbumID ALBUMS.AlbumID%TYPE := 2;
  rec_albums ALBUMS%ROWTYPE;
BEGIN
    music_library_pkg.get_albums(lv_AlbumID, rec_albums);
    DBMS_OUTPUT.PUT_LINE('Album ID: ' || rec_albums.AlbumID ||  ' Album Name: ' || rec_albums.AlbumName ||  ' Artist ID: ' || rec_albums.ArtistID);
END;
/

-- UPDATE ALBUMS
BEGIN
music_library_pkg.update_albums('Off the Wall', 2, 1);
END;
/

SELECT * FROM ALBUMS
    WHERE AlbumID = 2;

-- DELETE ALBUMS
BEGIN 
    music_library_pkg.delete_albums(5);
END;
/
SELECT * FROM ALBUMS;


-- CREATE SONGS
BEGIN
  music_library_pkg.insert_SONGS(16, 'Sgt. Peppers', 3, 1, 'File Name', 12);
END;
/
SELECT * FROM SONGS;

-- READ SONGS
DECLARE
  lv_SongID SONGS.SongID%TYPE := 2;
  rec_songs SONGS%ROWTYPE;
BEGIN
    music_library_pkg.get_songs(lv_SongID, rec_songs);
    DBMS_OUTPUT.PUT_LINE('Song ID: ' || rec_songs.SongID ||  ' Song Name: ' || rec_songs.SongTitle);
END;
/

-- UPDATE SONGS
BEGIN
music_library_pkg.update_songs(4, 'Beat it', 1, 2, 'FileName', 1);
END;
/

SELECT * FROM SONGS
    WHERE SongID = 4;

-- DELETE SONGS
BEGIN 
    music_library_pkg.delete_songs(5);
END;
/
SELECT * FROM SONGS;


-- CREATE PLAYLISTS
BEGIN
  music_library_pkg.insert_playlists(13, 'Summer Jams', 1, 1);
END;
/

SELECT * FROM PLAYLISTS;

-- READ PLAYLISTS
DECLARE
  lv_playlistID PLAYLISTS.playlistID%TYPE := 2;
  rec_playlists PLAYLISTS%ROWTYPE;
BEGIN
    music_library_pkg.get_playlists(lv_playlistID, rec_playlists);
    DBMS_OUTPUT.PUT_LINE('Playlist ID: ' || rec_playlists.PlaylistID ||  ' Playlist Name: ' || rec_playlists.PlaylistName);
END;
/

-- UPDATE SONGS
BEGIN
music_library_pkg.update_playlists(3, 'Everyones Favorites', 3, 2);
END;
/
SELECT * FROM PLAYLISTS
    WHERE PlaylistID = 3;

-- DELETE PLAYLISTS
BEGIN 
    music_library_pkg.delete_playlists(1);
END;
/

SELECT * FROM PLAYLISTS;


-- CREATE PLAYED
BEGIN
  music_library_pkg.insert_played(5, 3, sysdate);
END;
/

SELECT * FROM PLAYED;

-- READ PLAYED
DECLARE
  lv_songID PLAYED.SongID%TYPE := 3;
  rec_played PLAYED%ROWTYPE;
BEGIN
    music_library_pkg.get_played(lv_songID, rec_played);
    DBMS_OUTPUT.PUT_LINE('Played Song ID: ' || rec_played.SongID ||  'User ID: ' || rec_played.UserID);
END;
/

-- UPDATE PLAYED
BEGIN
music_library_pkg.update_played(2, 3, sysdate);
END;
/

SELECT * FROM PLAYED
    WHERE SongID = 2;

-- DELETE PLAYED

BEGIN
music_library_pkg.delete_played(5);
END;
/

SELECT
    *
FROM
    played;
    

begin 
suggested_song(1);
end;
/

select * from suggested where userid = 1;